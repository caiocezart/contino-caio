FROM nginxdemos/hello:latest
ARG COMMIT_SHA='test'

RUN rm /etc/nginx/conf.d/*

RUN echo $'\n\
server { \n\
    listen 80;\n\
    root /usr/share/nginx/html;\n\
    try_files /index.html =404;\n\
    expires -1;\n\
    sub_filter_once off;\n\
    sub_filter \'commit_sha\' \''$COMMIT_SHA$'\';\n\
    sub_filter \'server_hostname\' \'$hostname\';\n\
    sub_filter \'server_address\' \'$server_addr:$server_port\';\n\
    sub_filter \'server_url\' \'$request_uri\';\n\
    sub_filter \'server_date\' \'$time_local\';\n\
    sub_filter \'request_id\' \'$request_id\';\n\
}' >> /etc/nginx/conf.d/hello.conf
    
ADD index.html /usr/share/nginx/html/