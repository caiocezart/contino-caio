ASSUME_REQUIRED?=.env.assume

ifdef CI
	ECR_REQUIRED?=
else
	ECR_REQUIRED?=ecrLogin
endif

BUILD_VERSION?=latest
ifdef CI_COMMIT_TAG
	BUILD_VERSION=${CI_COMMIT_TAG}
endif

ECR_ACCOUNT?=649775121514
AWS_ACCOUNT_ID?=649775121514
ECR_REGION?=ap-southeast-2
CLUSTER_NAME?=caio

APP_NAME=portal
BUILD_VERSION?=latest
BUILD_TAG?=latest
IMAGE_NAME=$(ECR_ACCOUNT).dkr.ecr.$(ECR_REGION).amazonaws.com/$(APP_NAME):$(BUILD_VERSION)

# Variables used inside Docker
export APP_NAME
export BUILD_VERSION
export IMAGE_NAME

# Check for specific environment variables
env-%:
	@ if [ "${${*}}" = "" ]; then \
		echo "Environment variable $* not set"; \
		exit 1; \
	fi

dockerBuild:
	@echo "make dockerBuild"
	docker build --no-cache --build-arg COMMIT_SHA=$(BUILD_VERSION) -t $(IMAGE_NAME) .
.PHONY: dockerBuild

dockerPush: $(ECR_REQUIRED)
	echo "make dockerPush"
	docker push $(IMAGE_NAME)
.PHONY: dockerPush

deploy: $(ASSUME_REQUIRED)
	@echo "make deploy"
	docker-compose run --rm deploy ./deploy.sh
.PHONY: deploy

shell-aws: $(ASSUME_REQUIRED)
	@echo "make deploy"
	docker-compose run --rm aws /bin/bash
.PHONY: deploy
